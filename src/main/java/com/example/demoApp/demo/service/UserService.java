package com.example.demoApp.demo.service;

import com.example.demoApp.demo.dto.request.CarDTO;
import com.example.demoApp.demo.dto.request.UserDTO;
import com.example.demoApp.demo.dto.response.GeneralResponseDTO;
import com.example.demoApp.demo.model.Car;
import com.example.demoApp.demo.model.User;
import com.example.demoApp.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CarService carService;

    public ResponseEntity getAllUsers() {
        List<UserDTO> userDTOS = userRepository.findAll().stream().map(UserDTO::new).collect(Collectors.toList());
        List<Car> cars;

        for(UserDTO user: userDTOS) {
            cars = carService.findByUserId(user.getId());
            user.setCars(cars.stream().map(CarDTO::new).collect(Collectors.toList()));
        }

        return new ResponseEntity<>(userDTOS, HttpStatus.OK);
    }

    public ResponseEntity createUser(UserDTO userDTO) {
        if (checkNotEquals(userDTO.getPassword(), userDTO.getRepeatedPassword())) {
            return new ResponseEntity<>("Passwords do not match.", HttpStatus.BAD_REQUEST);
        }

        User user = new User(userDTO);
        if (checkMail(userDTO.getEmail(), null)) {
            return new ResponseEntity<>("Email already exists.", HttpStatus.OK);
        }

        try {
            userRepository.save(user);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.OK);
        }

        if (userDTO.getCars() != null) {
            userDTO.getCars().forEach(carDTO -> {
                Car car = new Car(carDTO);

                car.setUser(user);
                carService.saveCar(car);
            });
        }

        return new ResponseEntity<>("User created successfully.", HttpStatus.OK);
    }

    public GeneralResponseDTO updateUser(long id, UserDTO userDTO) {
        User user = userRepository.findOne(id);

        if (user == null) {
            return new GeneralResponseDTO(HttpStatus.NOT_FOUND.value(), "User with given ID does not exist", id);
        }

        user = User.update(user, userDTO);
        if (checkMail(userDTO.getEmail(), user)) {
            return new GeneralResponseDTO(HttpStatus.OK.value(), "Email already exists.", user);
        }

        try {
            userRepository.save(user);
        } catch (Exception e) {
            return new GeneralResponseDTO(HttpStatus.BAD_REQUEST.value(), e.getMessage(), user);
        }

        return new GeneralResponseDTO(HttpStatus.OK.value(), "User successfully updated.", user);
    }

    public ResponseEntity deleteUser(long id) {
        try {
            userRepository.delete(id);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.OK);
        }

        return new ResponseEntity<>("User deleted successfully", HttpStatus.OK);
    }

    public User getUser(long id) {
        return userRepository.findOne(id);
    }

    /**
     * Checks for equal passwords.
     *
     * @param first  - String first password.
     * @param second - String second repeat password.
     * @return       - Whether strings are equal.
     */
    private boolean checkNotEquals(String first, String second) {
        return !first.equals(second);
    }

    /**
     * Checks if mail can be assigned to existing or new {@link User}.
     *
     * @param email        - String email to be checked.
     * @param existingUser - Existing {@link User} to be checked for mail.
     * @return             - Whether email can be assigned to {@link User}.
     */
    private boolean checkMail(String email, User existingUser) {
        User user = userRepository.findByEmail(email);

        if (existingUser == null) {
            return user != null;
        } else {
            return existingUser.getId() != user.getId();
        }
    }

}
