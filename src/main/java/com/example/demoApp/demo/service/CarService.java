package com.example.demoApp.demo.service;

import com.example.demoApp.demo.dto.request.CarDTO;
import com.example.demoApp.demo.dto.response.GeneralResponseDTO;
import com.example.demoApp.demo.model.Car;
import com.example.demoApp.demo.model.User;
import com.example.demoApp.demo.repository.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CarService {

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private UserService userService;

    /**
     * Returns all {@link Car}s mapped into {@link CarDTO}s that exist in database.
     */
    public ResponseEntity getCars() {
        List<CarDTO> carDTOs = carRepository.findAll().stream().map(CarDTO::new).collect(Collectors.toList());
        return new ResponseEntity<>(carDTOs, HttpStatus.OK);
    }

    /**
     * Create car from userId information and given {@link CarDTO} body.
     *
     * @param userId - {@link User} id that will get new car attached to it.
     * @param carDTO - {@link CarDTO} body with information to create new {@link Car}.
     * @return       - {@link ResponseEntity} with information about creating new {@link Car}.
     */
    public ResponseEntity create(long userId, CarDTO carDTO) {
        User user = userService.getUser(userId);
        if (user == null) {
            return new ResponseEntity<>("User with given ID does not exist", HttpStatus.OK);
        }

        Car car = new Car(carDTO);
        car.setUser(user);

        try {
            carRepository.save(car);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.OK);
        }

        return new ResponseEntity<>(new GeneralResponseDTO(HttpStatus.OK.value(), "Car created successfully", new CarDTO(car)), HttpStatus.OK);
    }

    /**
     * Updates existing {@link Car} with given id from {@link CarDTO} data.
     *
     * @param id     - Id of the {@link Car} that will be updated.
     * @param carDTO - Data for the {@link Car} to be updated with {@link CarDTO} data.
     * @return       - {@link ResponseEntity} depending on the action result.
     */
    public ResponseEntity update(long id, CarDTO carDTO) {
        Car car = carRepository.findOne(id);

        if (car == null) {
            return new ResponseEntity<>("Car with given ID does not exist", HttpStatus.OK);
        }

        car = Car.update(car, carDTO);

        try {
            carRepository.save(car);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>("Car updated successfully", HttpStatus.OK);
    }

    /**
     * Delete {@link Car} from database with given id.
     *
     * @param id - Id of a {@link Car} that should be deleted.
     * @return   - {@link ResponseEntity} with deleting result.
     */
    public ResponseEntity delete(long id) {
        try {
            carRepository.delete(id);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.OK);
        }
        return new ResponseEntity<>("Car deleted successfully", HttpStatus.OK);
    }

    /**
     * Finds {@link Car}s by given user Id.
     *
     * @param userId - {@link User} id used for finding list of all {@link Car}s.
     * @return       - Returns List of found {@link Car}s.
     */
    public List<Car> findByUserId(long userId) {
        return carRepository.findByUserId(userId);
    }

    /**
     * Saves {@link Car} to database.
     *
     * @param car - {@link Car} to be saved.
     */
    public void saveCar(Car car) {
        carRepository.save(car);
    }
}
