package com.example.demoApp.demo.model;

import com.example.demoApp.demo.dto.request.CarDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table(name = "car")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Car implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotNull
    private long built;

    @NotBlank
    @NotNull
    @Size(min = 8, max = 255)
    private String registrationNumber;

    @NotNull
    @Size(min = 8, max = 255)
    private String numberOfChassis;

    @ManyToOne
    @NotNull
    private User user;

    public Car(CarDTO carDTO) {
        this.built = carDTO.getBuilt();
        this.registrationNumber = carDTO.getRegistrationNumber();
        this.numberOfChassis = carDTO.getNumberOfChassis();
    }

    static public Car update(Car car, CarDTO carDTO) {
        car.numberOfChassis = carDTO.getNumberOfChassis();
        car.registrationNumber = carDTO.getRegistrationNumber();
        car.built = carDTO.getBuilt();

        return car;
    }
}
