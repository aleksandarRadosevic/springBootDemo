package com.example.demoApp.demo.model;

import com.example.demoApp.demo.dto.request.UserDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.util.List;

/**
 * DAO (Data access object) used as domain model that will be saved to database.
 * Contains basic information about {@link User} with unique email, and password used for logging in.
 * <ul>
 * <li> Data               - annotation generates getters and setters for {@link User} fields. </li>
 * <li> NoArgsConstructor  - annotation generates constructor without arguments. </li>
 * <li> AllArgsConstructor - annotation generates constructor with all arguments. </li>
 * </ul>
 */
@Entity
@Table(name = "user")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {

    /**
     * Unique identifier for database.
     * It is using auto - generated strategy and is not controlled by user input.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    /**
     * Username used for logging in, cannot be null or blank.
     */
    @NotBlank
    private String username;

    /**
     * Password used for logging in, cannot be null or blank.
     */
    @NotBlank
    private String password;

    /**
     * Email that can be alternative for username for logging, cannot be null or blank.
     */
    @NotBlank
    private String email;

    private String name;

    private String lastName;

    private String address;

    private long dateOfBirth;

    /**
     * List of all {@link List<Car>} that this user has in database.
     * Cascade - Will delete all {@link List<Car>} from database when user is being deleted.
     */
    @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE)
    private List<Car> cars;

    /**
     * Constructor used for mapping {@link User} from {@link UserDTO}.
     *
     * @param userDTO - {@link UserDTO} that will be used for mapping User.
     */
    public User(UserDTO userDTO) {
        this.username = userDTO.getUsername();
        this.password = userDTO.getPassword();
        this.email = userDTO.getEmail();
    }

    /**
     * Static method used for updating existing user.
     *
     * @param user    - {@link User} that will be updated.
     * @param userDTO - Data in form of {@link UserDTO} that will be used for updating.
     * @return - Updated {@link User}.
     */
    static public User update(User user, UserDTO userDTO) {
        user.username = userDTO.getUsername();
        user.password = userDTO.getPassword();
        user.email = userDTO.getEmail();

        return user;
    }
}
