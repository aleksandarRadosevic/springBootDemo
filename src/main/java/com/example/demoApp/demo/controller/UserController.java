package com.example.demoApp.demo.controller;

import com.example.demoApp.demo.dto.request.UserDTO;
import com.example.demoApp.demo.dto.response.GeneralResponseDTO;
import com.example.demoApp.demo.service.UserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api")
public class UserController {

    @Inject
    private UserService userService;

    @RequestMapping(method = RequestMethod.GET, value = "/helloWorld")
    public ResponseEntity helloWorld() {
        return new ResponseEntity<>("Hello World!", HttpStatus.OK);
    }

    @ApiOperation("Api is used for listing all users with their child tables.")
    @RequestMapping(method = RequestMethod.GET, value = "/users")
    public ResponseEntity read() {
        return userService.getAllUsers();
    }

    @ApiOperation("Api is used for creating user.")
    @RequestMapping(method = RequestMethod.POST, value = "/user")
    public ResponseEntity create(@RequestBody @Valid UserDTO userDTO) {
        return userService.createUser(userDTO);
    }

    @ApiOperation("Api is used for updating user.")
    @RequestMapping(method = RequestMethod.PUT, value = "/user/{id}")
    public GeneralResponseDTO update(@PathVariable long id, @RequestBody @Valid UserDTO userDTO) {
        return userService.updateUser(id, userDTO);
    }

    @ApiOperation("Api is used for deleting user.")
    @RequestMapping(method = RequestMethod.DELETE, value = "/user/{id}")
    public ResponseEntity delete(@PathVariable long id) {
        return userService.deleteUser(id);
    }
}