package com.example.demoApp.demo.controller;

import com.example.demoApp.demo.dto.request.CarDTO;
import com.example.demoApp.demo.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api")
public class CarController {

    @Autowired
    private CarService carService;

    @RequestMapping(method = RequestMethod.GET, value = "/cars")
    public ResponseEntity read() {
        return carService.getCars();
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{userId}/car")
    public ResponseEntity create(@PathVariable long userId, @RequestBody @Valid CarDTO carDTO) {
        return carService.create(userId, carDTO);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/car/{id}")
    public ResponseEntity update(@PathVariable long id, @RequestBody @Valid CarDTO carDTO) {
        return carService.update(id, carDTO);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/car/{id}")
    public ResponseEntity delete(@PathVariable long id) {
        return carService.delete(id);
    }
}
