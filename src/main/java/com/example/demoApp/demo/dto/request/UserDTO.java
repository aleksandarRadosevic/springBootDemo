package com.example.demoApp.demo.dto.request;

import com.example.demoApp.demo.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * DTO of user used for creating new users as well as mapping existing ones for response.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {

    private long id;

    @Size(max = 40)
    private String username;

    @NotNull
    private String password;

    @NotNull
    @NotBlank
    private String repeatedPassword;

    @Email
    private String email;

    @Valid
    private List<CarDTO> cars;

    public UserDTO(User user) {
        this.id = user.getId();
        this.username = user.getUsername();
        this.password = user.getPassword();
        this.email = user.getEmail();
    }
}
