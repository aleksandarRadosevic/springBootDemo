package com.example.demoApp.demo.dto.request;

import com.example.demoApp.demo.model.Car;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CarDTO implements Serializable {

    private long id;

    @NotNull
    @NotBlank
    private String registrationNumber;

    @NotNull
    @NotBlank
    private String numberOfChassis;

    @NotNull
    private long built;

    public CarDTO(Car car) {
        this.id = car.getId();
        this.registrationNumber = car.getRegistrationNumber();
        this.numberOfChassis = car.getNumberOfChassis();
        this.built = car.getBuilt();
    }
}
