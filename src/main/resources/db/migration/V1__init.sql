# DROP DATABASE IF EXISTS sep;
#
# CREATE SCHEMA sep;
#
# USE sep;

CREATE TABLE user (
  id BIGINT(20) NOT NULL AUTO_INCREMENT,
  name VARCHAR(40),
  last_name VARCHAR(40),
  address VARCHAR(255),
  email VARCHAR(50) NOT NULL,
  username VARCHAR(40) NOT NULL,
  password VARCHAR(255) NOT NULL,
  date_of_birth BIGINT(20),
  PRIMARY KEY (id),
  UNIQUE KEY UK_email (email)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE car (
  id BIGINT(20) NOT NULL AUTO_INCREMENT,
  built BIGINT(20) NOT NULL,
  registration_number VARCHAR(255) NOT NULL,
  number_of_chassis VARCHAR(255) NOT NULL,
  user_id BIGINT(20) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (user_id) REFERENCES user (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;